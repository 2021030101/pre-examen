const router = require('express').Router();
const Recibo = require('../models/recibo.js');

router.get('/luz', (req, res) => {
    res.render('index', {
        pagina: 'entrada',
    });
});

router.post('/luz', (req, res) => {
    const recibo = new Recibo(req.body.numero,
                              req.body.nombre,
                              req.body.domicilio,
                              req.body.servicio,
                              req.body.kilowatts);
    res.render('index', {
        pagina: 'salida',
        recibo: recibo,
    });
});

module.exports = router;