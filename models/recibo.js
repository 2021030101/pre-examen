class Recibo {
    constructor(numero, nombre, domicilio, servicio, kilowatts) {
        this.numero = numero;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.servicio = servicio;
        this.kilowatts = kilowatts;
        this.precio = servicio === "1" ? 1.08 : 
                      servicio === "2" ? 2.5:
                      servicio === "3" ? 3 : 0;
    }

    get subtotal() {
        return this.precio * this.kilowatts;
    }

    get descuento() {
        const subtotal = this.subtotal;
        return this.kilowatts <= 1000 ? subtotal * 0.10 :
               this.kilowatts >= 1000 && this.kilowatts <= 10000 ? subtotal * 0.20:
               this.kilowatts > 10000 ? subtotal * 0.50 : 0;
    }

    get total() {
        return (this.subtotal - this.descuento) + this.subtotal * 0.16;
    }
}

module.exports = Recibo;