const limpiar = document.querySelector('.limpiar');
const inputs = document.querySelectorAll('input');

limpiar.addEventListener('click', e => {
    e.preventDefault();
    inputs.forEach(input => input.value = '');
});